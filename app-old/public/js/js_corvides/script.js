document.querySelector(".card_article_titre").animate([
    // keyframes
    { transform: 'scale(0)' },
    { transform: 'opacity: 1' }
], {
    // timing options
    duration: 500,
    iterations:1
});
document.querySelector(".card_article_text").animate([
    // keyframes
    { transform: 'translateX(-1000px)' },
    { transform: 'translateX(0x)' }

], {
    // timing options
    duration: 500,
    iterations:1
});

