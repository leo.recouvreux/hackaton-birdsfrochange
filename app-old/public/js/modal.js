
let modal = null

const openModal = function (target, zIndex) {
    modal = document.querySelector(target)
    modal.style.display = null
    modal.removeAttribute('aria-hidden')
    modal.setAttribute('aria-modal', 'true')
    // modal.addEventListener('click', closeModal)
    modal.querySelector('.js-modal-close').addEventListener('click', closeModal)
    modal
        .querySelector('.js-modal-stop')
        .addEventListener('click', stopPropagation)
    modal.style.zIndex = zIndex
}

function closeModal(e) {
    let popup = e.target.parentNode.parentNode.parentNode.parentNode

    if (modal === null) return
    e.preventDefault()

    window.setTimeout(function () {
        popup.style.display = 'none'
        popup = null
    }, 500)

    popup.setAttribute('aria-hidden', 'true')
    popup.removeEventListener('click', closeModal)
    popup
        .querySelector('.js-modal-close')
        .removeEventListener('click', closeModal)
    popup
        .querySelector('.js-modal-stop')
        .removeEventListener('click', stopPropagation)

    document.querySelector('body').removeChild(popup)
}

const stopPropagation = function (e) {
    e.stopPropagation()
}

window.addEventListener('keydown', function (e) {
    if (e.key === 'Escape' || e.key == 'Esc') {
        closeModal(e)
    }
})

/**
 * Cette fonction permet la creation d'un popup avec un contenu, un titre, et un z-index afin de permettre la superposition
 * @param {string} modal_title
 * @param {string} modal_id
 * @param {Node} content
 * @param {int} zIndex
 */
function createBasicPopup(modal_title, modal_id, content, zIndex) {
    const ASIDE = document.createElement('aside')
    ASIDE.id = modal_id
    ASIDE.classList.add('modal')
    ASIDE.classList.add('task-details-modal')
    ASIDE.ariaHidden = 'true'
    ASIDE.setAttribute('role', 'dialog')
    ASIDE.setAttribute('aria-labelledby', 'modal-title')
    ASIDE.style.display = 'none'

    const modalWrapper = document.createElement('div')
    modalWrapper.classList.add('modal-wrapper')
    modalWrapper.classList.add('js-modal-stop')
    modalWrapper.id = `modalWrapper${modal_id}`
    const modalHeader = document.createElement('div')
    modalHeader.classList.add('mod-header')

    const modalCloseButton = document.createElement('button')
    modalCloseButton.classList.add('js-modal-close')
    modalCloseButton.type = 'button'
    modalCloseButton.ariaLabel = 'Fermer'

    const modalI = document.createElement('i')
    modalI.classList.add('far')
    modalI.classList.add('fa-times-circle')
    const modalTitle = document.createElement('h2')
    modalTitle.id = 'modal_title'
    modalTitle.innerText = modal_title

    modalCloseButton.appendChild(modalI)
    modalHeader.appendChild(modalCloseButton)
    modalHeader.appendChild(modalTitle)

    modalWrapper.appendChild(modalHeader)
    modalWrapper.appendChild(content)
    ASIDE.appendChild(modalWrapper)
    BODY.appendChild(ASIDE)
    openModal(`#${modal_id}`, zIndex)
}