document.querySelector(".h2-media").animate([
    // keyframes
    { transform: 'translateX(500px)' },
    { transform: 'translateX(0x)' }
], {
    // timing options
    duration: 600,
    iterations:1
});
document.querySelector(".containerCard").animate([
    // keyframes
    { transform: 'translateX(-500px)' },
    { transform: 'translateX(0x)' }
], {
    // timing options
    duration: 600,
    iterations:1
});
