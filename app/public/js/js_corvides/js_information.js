document.querySelector(".about_animation").animate([
    // keyframes
    { transform: 'scale(0.5)' },
    { transform: 'opacity: 0' }
], {
    // timing options
    duration: 500,
    iterations:1
});